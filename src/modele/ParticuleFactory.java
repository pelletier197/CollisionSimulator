package modele;

import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.paint.Color.BLUE;
import static javafx.scene.paint.Color.BROWN;
import static javafx.scene.paint.Color.GREEN;
import static javafx.scene.paint.Color.PINK;
import static javafx.scene.paint.Color.RED;
import static javafx.scene.paint.Color.YELLOW;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class ParticuleFactory {
	
	 private static final Color[] COLORS = new Color[] { RED, YELLOW, GREEN,
         BROWN, BLUE, PINK, BLACK, Color.BURLYWOOD, Color.DARKORANGE, Color.FIREBRICK, Color.WHEAT, Color.SANDYBROWN, Color.MEDIUMPURPLE, Color.MEDIUMSLATEBLUE, Color.GRAY, Color.MOCCASIN };

	private DoubleProperty radiusProperty;
	private DoubleBinding angleProperty;
	private DoubleProperty speedProperty;
	private ObjectProperty<Color> color;

	private CollisionnerManager manager;
	/**
	 * La vitesse s'exprime en pixel par seconde. La valeur du slider variant de
	 * 0 à 10, on multiplie la vitesse en pixel par seconde par cette valeur,
	 * leur donnant une vitesse intéressante.
	 */
	private static final double speedMultiplicatorFactor = 400d;

	public ParticuleFactory(DoubleProperty radius, DoubleProperty angle,
			DoubleProperty speed, ObjectProperty<Color> color, CollisionnerManager manager) {
		if (radius != null && angle != null && speed != null && manager != null) {
			
			this.radiusProperty = radius;
			this.angleProperty = angle.multiply(-1*Math.PI).divide(180);
			this.speedProperty = speed;
			this.manager = manager;
			this.color = color;
			
		} else {
			throw new NullPointerException();
		}
	}
	
	/** crée le cercle
	 * 
	 * @param particule
	 * @param couleur
	 * @return
	 */
	private synchronized Circle creeCercle(Particule p, Color couleur){
		
		Circle c = new Circle(p.getRadius());
		c.setFill(couleur);
		c.centerXProperty().bind((p.positionXProperty()));
		c.centerYProperty().bind(p.positionYProperty());
		
		DropShadow shadow = new DropShadow(5, Color.BLACK);
		
		 shadow.setOffsetX(3.0);
		 shadow.setOffsetY(3.0);
	   	 c.setEffect(shadow);
	   	 
	   	 manager.addParticule(p);
		return c;	
	}
	
	public synchronized Circle generateParticule(double posX, double posY ){
		
		
		//crée la particule
		Particule p = new Particule(posX, posY, radiusProperty.get());
		p.setSpeedForce(speedProperty.get()*speedMultiplicatorFactor);
		p.setAngle(angleProperty.get());
		
		return creeCercle(p,color.get());
		
	}

	public synchronized List<Circle> generateMultipleParticules(double maxX, double maxY) {
		Random rnd = new Random();
		int nombre = (rnd.nextInt(15))+1;
		ArrayList<Circle> listeparticule = new ArrayList<Circle>();
		
		
		
		for(int i =0; i<nombre;i++){
			//crée la particule
			Particule p = new Particule(rnd.nextDouble()*maxX, rnd.nextDouble()*maxY, (rnd.nextDouble()*10)+4);
			p.setSpeedForce((rnd.nextDouble()*10)*speedMultiplicatorFactor);
			p.setAngle(rnd.nextDouble()*(2*Math.PI));
			
		   	listeparticule.add(creeCercle(p,COLORS[i%COLORS.length]));
   	 
		}
		
		return listeparticule;
	}
}
