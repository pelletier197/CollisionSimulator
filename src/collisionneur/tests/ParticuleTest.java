package collisionneur.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import collisionneur.modele.Particule;

public class ParticuleTest {

	private Particule part;

	@Before
	public void before() {
		part = new Particule(0, 0, 1);
	}

	@Test
	public void testParticule() {
		try {
			part = new Particule(0, 0, 1);

		} catch (Exception e) {
			fail();
		}
		try {
			part = new Particule(0, 0, 0);
			fail();
		} catch (Exception e) {

		}
		try {
			part = new Particule(0, -1, 1);
			fail();
		} catch (Exception e) {

		}
		try {
			part = new Particule(-1, 0, 1);
			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testSetSpeedForce() {

		part.setAngle(0);
		part.setSpeedForce(500);

		assertTrue(part.getVelocityX() == 500);

	}

	@Test
	public void testGetAngle() {
		part.setAngle(0);
		part.setSpeedForce(500);

		assertTrue(part.getVelocityX() == 500);

		part.setAngle(Math.PI);

		assertTrue(Math.abs(part.getVelocityX() + 500) < 1);
	}

	@Test
	public void testUpdatePosition() {
		part.setVelocityVector(50, 50);

		assertTrue(part.getPositionX() == 0);
		assertTrue(part.getPositionY() == 0);
		// 1000ms
		part.updatePosition(1000);
		assertTrue(part.getPositionX() - 50 < 1);
		assertTrue(part.getPositionY() - 50 < 1);
	}

}
