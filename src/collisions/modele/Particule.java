package collisions.modele;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;

import com.sun.javafx.geom.Vec2d;


public class Particule {

	private Color color;
	private DoubleProperty positionX;
	private DoubleProperty positionY;
	/**
	 * Rayon de la particule. agit à la fois comme gestionnaire de la masse de
	 * la particules, car considère que les particules ont tous la même densité.
	 */
	private double radius;
	/**
	 * Vecteur de la velocité de la particule. La vélocité agit comme le vecteur
	 * vitesse, et sera determinant dans le déplacement de la particule. La
	 * norme de ce vecteur est égale à la vitesse de la particule. Cette
	 * vélocité dépend également du rayon de la particule. Deux particules
	 * possédant la même forceVitesse n'auront pas nécessairement la même
	 * vélocité.
	 */
	private Vec2d velocity;
	/**
	 * La force de vitesse donnée à la particule.
	 */
	private DoubleProperty speedForce;

	/**
	 * L'angle de la particule.
	 */
	private double angle;
	
	
	
	

	/**
	 * Constructeur d'une particule
	 * 
	 * @param positionX
	 *            Position de départ x de la particule.
	 * @param positionY
	 *            Position de départ y de la particule.
	 * @param radius
	 *            Le rayon de la particule.
	 */
	public Particule(double positionX, double positionY, double radius) {

		if (validBiggerThanZero(positionX) && validBiggerThanZero(positionY)
				&& validRadius(radius)) {

			this.positionX = new SimpleDoubleProperty(positionX);
			this.positionY = new SimpleDoubleProperty(positionY);
			this.speedForce = new SimpleDoubleProperty(1000);

			this.radius = radius;
			this.angle = Math.PI / 4;

			this.velocity = new Vec2d(
					(speedForce.get() / radius) * Math.cos(angle),
					(speedForce.get() / radius) * Math.sin(angle));

			this.color = Color.BLACK;
			

			initaliseListeners();
			

		}
	}

	
	

	/**
	 * Permet d'initialiser les listener sur la speedForce. Comme la force
	 * vitesse peut être bindée à une composante d'une vue, le vecteur vitesse
	 * de la particule est recalculé à chaque fois que la force vitesse est
	 * modifiée.
	 */
	private void initaliseListeners() {
		speedForce.addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {

				velocity.set(
						(newValue.doubleValue() / radius)
								* Math.cos(getAngle()),
						(newValue.doubleValue() / radius)
								* Math.sin(getAngle()));

			}
		});

	}

	/**
	 * Valide si le nombre est plus grand ou égal à 0.
	 * 
	 * @param number
	 *            Le nombre à vérifier.
	 */
	private boolean validBiggerThanZero(double number) {

		return number >= 0;
	}

	/**
	 * Valide si le rayon envoyé en paramètre est plus grand que zéro. Une
	 * particule ne peut pas avoir un rayon plus petit ou égal à 0.
	 * 
	 * @param radius
	 *            Le rayon à vérifier.
	 * @return vrai si le rayon est valide.
	 */
	private boolean validRadius(double radius) {

		return radius > 0;
	}

	/**
	 * Permet d'obtenir la propriété position x de la particule.
	 * 
	 */
	public final DoubleProperty positionXProperty() {
		return this.positionX;
	}

	/**
	 * Permet d'obtenir la valeur contenue dans la propriété position x.
	 * 
	 */
	public final double getPositionX() {
		return this.positionXProperty().get();
	}

	/**
	 * Permet d'obtenir la propriété position y de la particule.
	 * 
	 */
	public final DoubleProperty positionYProperty() {
		return this.positionY;
	}

	/**
	 * Permet d'obtenir la propriété speedForce de la particule. Cette propriété
	 * peut être bindée à un élément d'une vue ou tout autre propriété.
	 * 
	 */
	public final DoubleProperty speedForceProperty() {

		return this.speedForce;
	}

	/**
	 * Permet de donner une valeur à la speedForce.
	 */
	public void setSpeedForce(double value) {
		speedForce.set(value);
	}

	/**
	 * Permet d'obtenir la valeur contenue dans la propriété position y.
	 * 
	 */
	public final double getPositionY() {
		return this.positionYProperty().get();
	}

	/**
	 * Permet d'obtenir la valeur du rayon de la particule.
	 * 
	 * @return Le rayon de la particule.
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * Permet d'obtenir l'angle de la particule dans l'espace. Cet angle est
	 * calculé selon la vélocité de la particule.
	 * 
	 * @return L'angle de la particule en radians
	 */
	public double getAngle() {
		//Retourne l'angle réel demandé, et pas l'angle visuel
		return angle;
	}

	/**
	 * Permet de donner une valeur d'angle à la particule. Cet angle va modifier
	 * l'angle de la particule selon la variable speedForce, et non selon la
	 * norme du vecteur vitesse actuelle. Il se peut donc que si cette méthode
	 * est appelée, on voit la particule accélerer ou ralentir en changeant
	 * d'angle.
	 * 
	 * @param angle
	 *            Le nouvel angle de la particule en radians.
	 */
	public void setAngle(double angle) {
		this.angle = angle;

		// + MATH.PI pour que l'angle soit visiblement à l'écran égale à celui
		// demandé. Si on fait getAngle, l'angle sera de PI/4, et le sera aussi à l'écran
		velocity.set((speedForce.get() / radius) * Math.cos(this.angle),
				(speedForce.get() / radius) * Math.sin(this.angle));

	}

	/**
	 * Permet de changer la valeur de la couleur de la particule
	 * 
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * permet d'obtenir la couleur de la particule
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Permet d'obtenir la vélocité x de la particule. Cette vélocité a une
	 * valeur x égale au nombre de pixel qui sera parcouru par seconde par la
	 * particule à l'écran.
	 * 
	 * @return La valeur de la vélocité x.
	 */
	public double getVelocityX() {
		return velocity.x;
	}

	/**
	 * Permet d'obtenir la vélocité y de la particule. Cette vélocité a une
	 * valeur y égale au nombre de pixel qui sera parcouru par seconde par la
	 * particule à l'écran.
	 * 
	 * @return La valeur de la vélocité y.
	 */
	public double getVelocityY() {
		return velocity.y;
	}

	/**
	 * Permet de modifier la valeur de la vélocité x de la particule pour une
	 * nouvelle valeur.
	 */
	public void setVelocityX(double newValue) {
		this.velocity.x = newValue;
		computeAngle();

	}

	/**
	 * Permet de modifier la valeur de la vélocité y de la particule pour une
	 * nouvelle valeur.
	 */
	public void setVelocityY(double newValue) {
		this.velocity.y = newValue;
		computeAngle();

	}

	/**
	 * Permet de modifier la position x de la particule. L'appel de cette
	 * méthode ne modifie ni la vitesse ni l'angle de la particule.
	 */
	public void setPositionX(double newValue) {
		this.positionX.set(newValue);
	}

	/**
	 * Permet de modifier la position y de la particule. L'appel de cette
	 * méthode ne modifie ni la vitesse ni l'angle de la particule.
	 */
	public void setPositionY(double newValue) {
		this.positionY.set(newValue);
	}

	/**
	 * Permet de changer la vélocité du vecteur. On peut changer la vélocité du
	 * vecteur pour nimporte quelle valeur, mais une valeur trop élevé
	 * entraînerait un stagne de l'appication
	 * 
	 * @param newValueX
	 *            la nouvelle vélocité X
	 * @param newValueY
	 *            la nouvelle vélocité Y
	 */
	public void setVelocityVector(double newValueX, double newValueY) {

		velocity.set(newValueX, newValueY);
		computeAngle();

	}

	/**
	 * Permet d'updater les positions des particules selon le temps qui s'est
	 * écoulé depuis la dernière mise à jour. Le temps fait en sorte que les
	 * particules ne se déplacent pas trop vite. Comme le temps écoulé est
	 * généralement de l'ordre des milisecondes, si on bougeait de 10 pixel en x
	 * à chaque frame, on ne verrait jamais la particule à l'écran.
	 * 
	 * @param ellapsedTime
	 *            Le temps écoulé depuis la dernière frame.
	 */

	public void updatePosition(long ellapsedTime) {
		
		// Update la particule selon le temps en secondes écoulé et non en
		// miliardième de seconde.
		this.positionX.set(
				positionX.get() + velocity.x * ellapsedTime / 1_000_000_000.0);
		this.positionY.set(
				positionY.get() + velocity.y * ellapsedTime / 1_000_000_000.0);
	}

	/**
	 * Permet d'ajuter l'angle à chaque fois qu'on change le vecteur velocity.
	 * L'angle est toujours recalculé lors de la modification du vecteur,
	 */
	private void computeAngle() {
		// Si la vélocité est de 0, division impossible
		try {
			angle = Math.atan(this.velocity.y / this.velocity.x);
		} catch (ArithmeticException e) {
			// Ce qui veut dire que la particule va verticalement vers le haut
			// ou le bas
			if (velocity.y > 0) {
				angle = Math.PI;
			} else {
				angle = Math.PI * 3 / 2;
			}
		}

		// Comme atan, ne tient compte que de la partie de droite du cercle
		// trigonométrique, nous devons ajuster l'angle si x a une valeur
		// négative
		if (this.velocity.x < 0) {
			angle += Math.PI;
		}
		
	}
}
