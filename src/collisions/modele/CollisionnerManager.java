package collisions.modele;


import javafx.animation.AnimationTimer;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CollisionnerManager {

	private ReadOnlyDoubleProperty maxWidth;
	private ReadOnlyDoubleProperty maxHeight;

	private ListProperty<Particule> sizePropertyWatcher;
	private ObservableList<Particule> listeOfParticule;
	
	private AnimationTimer backgroundUpdater;

	public CollisionnerManager(ReadOnlyDoubleProperty maxWidth, ReadOnlyDoubleProperty maxHeight) {

		if (maxWidth == null || maxHeight == null)
			throw new NullPointerException();

		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
		listeOfParticule = FXCollections.observableArrayList();
		
		sizePropertyWatcher  = new SimpleListProperty<Particule>(listeOfParticule);
		initializeUpdater();
	}
	
	private void initializeUpdater() {
		/*
		 * Initialise le thread d'affichage et d'update des positions des
		 * particules
		 */

		final LongProperty lastUpdate = new SimpleLongProperty(0);

		backgroundUpdater = new AnimationTimer() {

			@Override
			public void handle(long now) {
				long ellapsed = now - lastUpdate.get();
				if (lastUpdate.get() > 0) {

					for (Particule p : listeOfParticule) {

						p.updatePosition(ellapsed);

					}
					for (Particule p : listeOfParticule) {

						verifierCollision(p);
					}
				}
				lastUpdate.set(now);

			}
		};
		backgroundUpdater.start();
		
	}
	

	/**
	 * Vérifie la collision entre les particules de la liste de particules, et
	 * la particule envoyée en paramètre. Si la colision est vrai, les
	 * trajectoires sont modifiées selon les lois établies.
	 * 
	 * @param p
	 *            La particule avec laquelle on souhaite vérifier les colisions.
	 */
	private void verifierCollision(Particule p) {
	
		/*
		 * Update les directions des particules si elles frapent un mur. On
		 * considère qu'une particule ne peut subir une colision contre le mur
		 * et une autre particule en même temps. On vérifie donc qu'une
		 * particule frappe un mur ou pas, et si elle n'en frappe pas, on ne
		 * cherche pas inutilement si elle frappe une autre particule
		 */
		if (!Physic.reboundAgainstWall(p, maxWidth.get(), maxHeight.get())) {
			double posX = 0.0;
			double posY = 0.0;
			double radius = 0.0;
	
			for (Particule part : listeOfParticule) {
				posX = part.getPositionX();
				posY = part.getPositionY();
				radius = part.getRadius();
				// S'assure que la particule actuelle n'est pas la particulle
				// elle-même. s'assure aussi que la particule avec laquelle on
				// colisionne ne déborde pas de la vue, sinon on la laisse
				// rentrer et on n'effectue aucune collision. La classe physic
				// replacera la particule.
				if (part != p && posX + radius <= maxWidth.get()
						&& posY + radius <= maxHeight.get()) {
					Physic.rebondir(p, part,
							part.getPositionX() - p.getPositionX(),
							part.getPositionY() - p.getPositionY());
	
				}
			}
	
		}
	
	}
	public double getMaxWidth(){
		return maxWidth.get();
	}
	public double getMaxHeight(){
		return maxHeight.get();
	}
	public final ReadOnlyIntegerProperty numberOfParticuleProperty(){
		return sizePropertyWatcher.sizeProperty();
	}
	public void addParticule(Particule p){
		listeOfParticule.add(p);
	}
	public void clearParticuleList(){
		listeOfParticule.clear();
	}
	

}
