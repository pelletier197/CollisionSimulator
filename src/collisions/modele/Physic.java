package collisions.modele;

import static java.lang.Math.sqrt;

import collisions.modele.Particule;

public class Physic {

	/**
	 * Permet de faire modifier l'angle de sortie d'une particule lorsqu'elle
	 * rebondit sur un mur. Il existe 4 cas distinct de rebond sur un mur.
	 * 
	 * - Si une particule rebondit sur le mur du haut et que son vecteur se
	 * dirige toujours vers le haut, sa vélocité y est dirigée vers le bas.
	 * 
	 * - Si une particule rebondit sur le mur du bas et que son vecteur se
	 * dirige toujours vers le bas, sa vélocité y est dirigée avec le haut.
	 * 
	 * - Si une particule rebondit sur le mur de droite et que son vecteur se
	 * dirige toujours vers la droite, sa vélocité x est dirigée vers la gauche.
	 * 
	 * - Si une particule rebondit sur le mur de gauche et que son vecteur se
	 * dirige toujours vers la gauche, sa vélocité x est dirigée vers la droite.
	 * 
	 * S'assure également qu'une particule ne déborde pas du maximum d'espace
	 * qui lui est attribué. Si cette particule dépasse, elle est repositionnée
	 * de facon à ce qu'elle ne dépasse pas de l'espace qui lui est fourni.
	 * 
	 * @return true si une colision a eu lieu avec un mur, false sinon.
	 * 
	 */

	public static boolean reboundAgainstWall(Particule p, double maxX,
			double maxY) {

		double posX = p.getPositionX();
		double posY = p.getPositionY();
		double radius = p.getRadius();

		boolean rebound = false;

		// Collision sur mur suppérieur ou inférieur

		if ((posY + radius > maxY)
				|| ((posY - radius < 0) && (p.getVelocityY() < 0))) {
			p.setVelocityVector(p.getVelocityX(), p.getVelocityY() * -1);
			rebound = true;
		}
		// Collision aux côtés droits ou gauche

		if (((posX - radius < 0) && (p.getVelocityX() < 0))
				|| ((posX + radius > maxX) && (p.getVelocityX() > 0))) {
			p.setVelocityVector(p.getVelocityX() * -1, p.getVelocityY());
			rebound = true;
		}
		// Gère le débordement replace les particules si elles dépassent de
		// l'espace max.
		if (posX + radius > maxX) {
			p.setPositionX(maxX - radius);
			rebound = true;
		}
		if (posX - radius < 0) {
			p.setPositionX(0 + radius);
			rebound = true;
		}
		if (posY + radius > maxY) {
			p.setPositionY(maxY - radius);
			rebound = true;
		}
		if (posY - radius < 0) {
			p.setPositionY(0 + radius);
			rebound = true;
		}
		return rebound;
	}

	/**
	 * * Vérifie si la collision entre les 2 particules est effectuée. La
	 * collision est vraie lorsque le rayons et les positions des particules se
	 * croisent, peu importe leurs positions dans le plan. Vérifie également si
	 * les deux particules sont en train de s'éloigner. Si les particules
	 * s'éloignent, la colision est fausse, car il serait autrement possible
	 * d'éffectuer 2 fois la collision durant une frame, ce qui ralentirait le
	 * processus.
	 * 
	 * @param p1
	 *            La première particule
	 * @param p2
	 *            La deuxième particule
	 * @param deltaX
	 *            Distance x entre les particules.
	 * @param deltaY
	 *            Distance y entre les particules.
	 * 
	 * @return Vrai si les deux particules sont en collision et que leurs
	 *         vecteurs se rapprochent.
	 */

	private static boolean colliding(Particule p1, Particule p2, double deltaX,
			double deltaY) {
		// square of distance between balls is s^2 = (x2-x1)^2 + (y2-y1)^2
		// balls are "overlapping" if s^2 < (r1 + r2)^2
		// We also check that distance is decreasing, i.e.
		// d/dt(s^2) < 0:
		// 2(x2-x1)(x2'-x1') + 2(y2-y1)(y2'-y1') < 0

		boolean retour = false;
		final double radiusSum = p1.getRadius() + p2.getRadius();
		if (deltaX * deltaX + deltaY * deltaY <= radiusSum * radiusSum) {
			if (deltaX * (p2.getVelocityX() - p1.getVelocityX()) + deltaY
					* (p2.getVelocityY() - p1.getVelocityY()) < 0) {
				retour = true;
			}
		}
		return retour;
	}

	/**
	 * Permet de faire rebondire les particules ensemble et de modifier le
	 * vecteur de sortie des 2 particules. Les collisions sont représentées par
	 * des colisions élastiques, dont l'énergie cinétique est conservée durant
	 * la colision. La vitesse résultante de chaque particule dépend également
	 * de la masse.
	 * 
	 * @param p1
	 *            La première particule
	 * @param p2
	 *            La deuxième particule
	 * @param deltaX
	 *            Distance en x entre la première et la deuxième particule.
	 * @param deltaY
	 *            Distance en y entre la première et la deuxième particule.
	 * 
	 * @see Source : https://gist.github.com/james-d/8327842
	 */
	public static void rebondir(Particule p1, Particule p2, double deltaX,
			double deltaY) {

		if (colliding(p1, p2, deltaX, deltaY)) {
			final double distance = sqrt(deltaX * deltaX + deltaY * deltaY);
			final double unitContactX = deltaX / distance;
			final double unitContactY = deltaY / distance;

			final double xVelocity1 = p1.getVelocityX();
			final double yVelocity1 = p1.getVelocityY();
			final double xVelocity2 = p2.getVelocityX();
			final double yVelocity2 = p2.getVelocityY();

			// Vélocité de la balle 1 parallèle à la balle deux
			final double u1 = xVelocity1 * unitContactX + yVelocity1
					* unitContactY;

			// Pareil pour balle 2
			final double u2 = xVelocity2 * unitContactX + yVelocity2
					* unitContactY;

			final double massSum = p1.getRadius() + p2.getRadius();
			final double massDiff = p1.getRadius() - p2.getRadius();

			// Colision à une dimension
			final double v1 = (2 * p2.getRadius() * u2 + u1 * massDiff)
					/ massSum;
			final double v2 = (2 * p1.getRadius() * u1 - u2 * massDiff)
					/ massSum;
			// On résout les équations pour la conservation de l'énergie et du
			// moment de force

			// Composant de la vélocité de la balle 1 perpendiculaire au vecteur
			// de contact.
			// Meme chose lors des collisions
			final double u1PerpX = xVelocity1 - u1 * unitContactX;
			final double u1PerpY = yVelocity1 - u1 * unitContactY;

			// Même chose pour la 2e balle
			final double u2PerpX = xVelocity2 - u2 * unitContactX;
			final double u2PerpY = yVelocity2 - u2 * unitContactY;

			// On change les vecteur et on s'assure qu'il aura la vitesse
			// nécessaire.
			p1.setVelocityVector(v1 * unitContactX + u1PerpX, v1 * unitContactY
					+ u1PerpY);
			p2.setVelocityVector(v2 * unitContactX + u2PerpX, v2  * unitContactY
					+ u2PerpY);

		}
	}
}
