package collisions.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

	private BorderPane root;
	private Scene scene;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		root = FXMLLoader.load(getClass().getResource(
				"/collisions/vue/Collisionner.fxml"));
		scene = new Scene(root);

		primaryStage.setTitle("TP3 - Collisions élastiques");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
