package collisions.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import collisions.modele.CollisionnerManager;
import collisions.modele.ParticuleFactory;

public class CollisionnerController implements Initializable {

	/**
	 * Le label affichant le nombre de particules présent dans la vue.
	 */
	@FXML
	private Label particuleCounterLabel;

	@FXML
	private Slider sliderVitesse;

	@FXML
	private Slider sliderAngle;

	@FXML
	private Slider sliderRayon;

	@FXML
	private ColorPicker ballColorPicker;

	@FXML
	private Pane paneSpace;

	private CollisionnerManager manager;
	
	private ParticuleFactory factory;



	/**
	 * Initialise l'application.
	 * 
	 * <li>Initialise la liste de particule</li> <li>Initialise le binding du
	 * label sur la size de la liste</li> <li>Initialise la couleur du
	 * colorPicker</li> <li>Bind le max et le min sur les bordures du pane</li>
	 * <li>Initialise le thread d'affichage et d'update des particules</li>
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		manager = new CollisionnerManager(paneSpace.widthProperty(),
				paneSpace.heightProperty());
		factory = new ParticuleFactory(sliderRayon.valueProperty(), sliderAngle.valueProperty(), sliderVitesse.valueProperty(),ballColorPicker.valueProperty(), manager);

		particuleCounterLabel.textProperty().bind(
				manager.numberOfParticuleProperty().asString());
		// @formatter:on;

		/*
		 * Définit la couleur par défaut du colorPicker
		 */
		ballColorPicker.setValue(Color.BLUE);
	}

	/**
	 * Appelé par la vue lorsque le bouton "Quitter" est clické. L'appel de
	 * cette méthode permet de fermer l,applicaiton javaFX courante
	 * 
	 * @param event
	 */
	@FXML
	private void exitClicked(ActionEvent event) {
		Platform.exit();
	}

	/**
	 * Appelé par la vue lorsque le bouton générer de la vue est clické. Permet
	 * de générer un nombre aléatoire de particules possédant un rayon, une
	 * couleur, et un angle aléatoire. Ces particules sont ajoutées à la vue et
	 * à la liste de particules interne.
	 * 
	 * @param event
	 */
	@FXML
	private void generateClicked(ActionEvent event) {

		paneSpace.getChildren().addAll(factory.generateMultipleParticules(paneSpace.getWidth(),paneSpace.getHeight()));
	}

	/**
	 * Appelé par la vue lorsque le bouton "Réinitialiser" est clické. Permet de
	 * vider la vue et la liste interne des particules. Les slider sont remis à
	 * leur état d'origine.
	 * 
	 * @param event
	 */
	@FXML
	private synchronized void resetClicked(ActionEvent event) {

		sliderVitesse.setValue(4);
		sliderAngle.setValue(0);
		sliderRayon.setValue(4);
		manager.clearParticuleList();
		paneSpace.getChildren().clear();
	}

	/**
	 * Appelé par la vue lorsqu'un utilisateur clique sur le pane de la vue.
	 * Permet de générer une particule avec l'angle du sliderAngle, la couleur
	 * du ballColorPicker et le rayon du sliderRayon.
	 * 
	 * @param event
	 */
	@FXML
	private void generateOnClick(MouseEvent event) {

		paneSpace.getChildren().add(factory.generateParticule(event.getX(),event.getY()));
		

	}

}
